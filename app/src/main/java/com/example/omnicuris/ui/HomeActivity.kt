package com.example.omnicuris.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ekoassignment.ui.viewModel.HomeViewModel
import com.example.omnicuris.R
import com.example.omnicuris.databinding.ActivityHomeBinding
import com.example.omnicuris.network.model.Question
import com.example.omnicuris.ui.adapter.QuestionAdapter

class HomeActivity : AppCompatActivity(), QuestionAdapter.onAnswerChangeListener {

    var questions: ArrayList<Question>? = null
    lateinit var binding: ActivityHomeBinding
    lateinit var adapter: QuestionAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        init()
    }

    private fun init() {
        getViewModel().getQuestion()
        startObservingLiveData()
        binding.submit.setOnClickListener{
            showMessage(submitQuiz())
        }
        binding.retry.setOnClickListener {
            retry()
        }
    }

    private fun submitQuiz():String {

        var point = 0
        if(questions!= null){


        for(i in questions!!){
            if(i.correctAns.equals(i.answerChose)){
                point++
            }
        }
            return "Your score is " + point + "/" + questions?.size
        }
        else
            return "Error occured"


    }

    private fun retry() {
        binding.failureContainer.visibility = View.GONE
        showProgress()
        getViewModel().getQuestion()
    }

    private fun startObservingLiveData() {
        getViewModel().todoResponse.observe(this, Observer {
            if (it.size > 0) {
                questions = it
                adapter = QuestionAdapter(questions, this)
                binding.quiz.layoutManager = LinearLayoutManager(this)
                binding.quiz.adapter = adapter
            } else {
                showMessage("Error Occured!!")
            }
            hideProgress()
        })

        getViewModel().isProgressRequired.observe(this, Observer {
            if (it)
                showProgress()
            else
                hideProgress()
        })
    }


    fun getViewModel(): HomeViewModel {
        return ViewModelProviders.of(this).get(HomeViewModel::class.java)
    }

    private fun showMessage(msg:String) {
        binding.errorMsg.text = msg
        binding.failureContainer.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        binding.progressContainer.visibility = View.GONE
    }

    private fun showProgress() {
        binding.progressContainer.visibility = View.VISIBLE
    }

    override fun onAnswerChanged(position: Int, answer: String) {
        questions?.get(position)?.answerChose = answer
    }
}
