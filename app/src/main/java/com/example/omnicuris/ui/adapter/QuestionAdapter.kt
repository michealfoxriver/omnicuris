package com.example.omnicuris.ui.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import com.example.omnicuris.R
import com.example.omnicuris.databinding.ListItemBinding
import com.example.omnicuris.network.model.Question

open class QuestionAdapter(var list:ArrayList<Question>?, var listener : onAnswerChangeListener): RecyclerView.Adapter<QuestionAdapter.QuestionItemHolder>() {

    init {
        list?.let {
            updateList(it)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuestionItemHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ListItemBinding>(
            layoutInflater, R.layout.list_item, parent, false, null
        )
        return QuestionItemHolder(binding,listener)
    }

    override fun getItemCount(): Int {
        return list?.size ?: 0
    }

    override fun onBindViewHolder(holder: QuestionItemHolder, position: Int) {
        val obj = list?.get(position)
        obj?.let { holder.bind(it) }
    }

    fun updateList(list: ArrayList<Question>?) {
        list?.let {
            this.list = it
            notifyDataSetChanged()
        }
    }


    class QuestionItemHolder(val binding: ListItemBinding, val listener: onAnswerChangeListener) : RecyclerView.ViewHolder(binding.root) {

        fun bind(obj: Question) {
            binding.setVariable(BR.obj, obj)
            binding.executePendingBindings()
            binding.answergrp.setOnCheckedChangeListener(object: RadioGroup.OnCheckedChangeListener{
                override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
                   var radioButton = group?.findViewById<RadioButton>(checkedId)
                    listener.onAnswerChanged(adapterPosition,radioButton?.text.toString())
                    }
            })
        }

    }

    public interface onAnswerChangeListener{
        fun onAnswerChanged(position: Int, answer:String)
    }
}
