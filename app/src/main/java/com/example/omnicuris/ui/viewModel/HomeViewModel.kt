package com.example.ekoassignment.ui.viewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.ekoassignment.network.Api
import com.example.ekoassignment.network.NetworkManager
import com.example.omnicuris.network.model.Question
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class HomeViewModel : ViewModel() {

    val todoResponse: MutableLiveData<ArrayList<Question>> = MutableLiveData()
    val isProgressRequired: MutableLiveData<Boolean> = MutableLiveData()


    fun getQuestion(){
        isProgressRequired.value = true
        NetworkManager.getInstance()
            .create(Api::class.java)
            .getQuestions().enqueue(object : Callback<ArrayList<Question>>{
                override fun onFailure(call: Call<ArrayList<Question>>, t: Throwable) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onResponse(call: Call<ArrayList<Question>>, response: Response<ArrayList<Question>>) {
                    todoResponse.value = response.body()
                }


            })
    }

}