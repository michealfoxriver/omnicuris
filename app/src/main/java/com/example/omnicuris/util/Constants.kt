package com.example.ekoassignment.util

object Constants {

    const val BASE_URL = "https://dl.dropboxusercontent.com/"
    const val GET_ALL_QUERY = "SELECT * FROM todos"
    const val DELETE_CONTENTS_QUERY = "DELETE FROM todos"


}