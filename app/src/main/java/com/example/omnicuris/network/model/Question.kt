package com.example.omnicuris.network.model

import com.google.gson.annotations.SerializedName

data class Question(@SerializedName("question") var question: String? = null,
                    @SerializedName("answer1")var answer1: String? = null,
                    @SerializedName("answer2")var answer2: String? = null,
                    @SerializedName("answer3")var answer3: String? = null,
                    @SerializedName("answer4")var answer4: String? = null,
                    @SerializedName("correctAns")var correctAns: String? = null,
                    var answerChose : String? = null)