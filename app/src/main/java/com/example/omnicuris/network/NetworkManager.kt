package com.example.ekoassignment.network

import com.example.ekoassignment.util.Constants
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NetworkManager {

    companion object{
        private var retrofit: Retrofit? = null

        fun getInstance() : Retrofit {
            if (retrofit == null){

                  retrofit = Retrofit.Builder()
                      .baseUrl(Constants.BASE_URL)
                      .addConverterFactory(GsonConverterFactory.create())
                      .build()
            }
            return  retrofit!!
        }

    }

}