package com.example.ekoassignment.network

import com.example.omnicuris.network.model.Question
import retrofit2.Call
import retrofit2.http.GET

interface Api {

    @GET("s/nccrd03cxyt1aan/sample.json?dl=0")
    fun getQuestions(): Call<ArrayList<Question>>
}